# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rcepre <rcepre@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/16 13:42:11 by rcepre           #+#    #+#              #
#    Updated: 2020/05/14 16:49:45 by rcepre           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
						HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

#COMPILER
CC =					clang
BASE_CFLAGS =			-Wall -Wextra -Werror -Wunreachable-code -Wwrite-strings
DEBUG_FLAGS =			-g3
RELEASE_FLAGS =			-O3 -DNDEBUG
LDFLAGS =

ifeq ($(RELEASE), 1)
	CFLAGS =			$(BASE_CFLAGS) $(RELEASE_FLAGS)
else
	CFLAGS =			$(BASE_CFLAGS) $(DEBUG_FLAGS)
endif

#NAME
NAME = 					libft_malloc.so
SYMLINK = 				libft_malloc_$(HOSTTYPE).so

#TESTS
TEST_EXECUTABLE =		test
TEST_DIR =				sources_test/
TEST_FILES =			test.c
SRCS_TEST =				$(addprefix $(TEST_DIR), $(TEST_FILES))


# SOURCES
SRCS =					$(addprefix $(SRCS_DIR), $(SRCS_FILES))
SRCS_DIR =				sources/
SRCS_FILES = 			malloc.c\
						calloc.c\
						free.c\
						utils.c\
						log.c\
						print_utils.c\
						realloc.c\
						show_alloc_mem.c\
						show_human_alloc_mem.c\
						getenv.c \
						infos.c

-include				lib_sources.mk

# HEADERS
HEADER_DIR =			headers/
HEADERS =				-I $(HEADER_DIR)

#OBJECTS
OBJS_DIR =				objects/
OBJS_NAME =				$(SRCS_FILES:.c=.o)
OBJS =					$(addprefix $(OBJS_DIR), $(OBJS_NAME))

#DEPENDENCIES
DEPS =					$(OBJS:%.o=%.d)

#RULES
all:  $(NAME)

$(NAME): $(SYMLINK)
	ln -sf $(SYMLINK) $@


$(SYMLINK): $(OBJS)
	@echo "Linking "
	$(CC) -shared $(CFLAGS) $(LDFLAGS)  -o $@ $^


$(OBJS_DIR)%.o: $(SRCS_DIR)%.c
	@echo "Compiling $<"
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -MMD $(HEADERS) -fPIC -o $@ -c $<


$(TEST_EXECUTABLE): $(SRCS_TEST) $(NAME)
	@echo "Compiling executable $(TEST_EXECUTABLE)"
	$(CC) $(BASE_CFLAGS) $(DEBUG_FLAGS) $(HEADERS) -o $@ $^


-include $(DEPS)

#CLEANING
clean:
	@echo "Clean .o .d"
	rm -rf $(OBJS_DIR)

fclean: clean
	@echo "Clean binaries"
	rm -rf $(NAME)
	rm -rf $(SYMLINK)
	rm -rf $(TEST_EXECUTABLE)
	rm -rf $(TEST_EXECUTABLE).dSYM

re: fclean all

.PHONY: all clean fclean re
