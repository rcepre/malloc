#include "libft.h"
#include <limits.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wctype.h>
#include <errno.h>

#ifndef NDEBUG
#define DEBUG 1
#endif

typedef unsigned short u_int16_t;

typedef unsigned int u_int32_t;

#define PAGE_SIZE getpagesize()

#define BLOCKS_PER_ZONE 123U /* Number of chunks in a tiny / small zone */

#define FULL (-1) /* Zone is full */

#define TINY_SIZE 64U
#define TINY_ZONE (TINY_SIZE * BLOCKS_PER_ZONE + sizeof(t_zone))

#define SMALL_SIZE 1024U
#define SMALL_ZONE (SMALL_SIZE * BLOCKS_PER_ZONE + sizeof(t_zone))

#define ZONE_OFFSET(zone) ((void *) (zone) + sizeof(t_zone)) /* Retrieve the location of the first chunk in a zone  */

#define PAGE_ALIGN(value) (((value) + (PAGE_SIZE - 1)) & -PAGE_SIZE) /* Align sizes to a multiple of PAGE_SIZE (4096) */

#define LARGE_OFFSET(zone) ((void *) (zone) + sizeof(t_large)) /* Retrieve the location of the data */

#define TYPE_TO_SIZE(type) (((type) == TINY) ? TINY_ZONE : SMALL_ZONE) /* Get the size of an zone from its type */

#define MMAP_FLAGS PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0 /* Flags and params for mmap() */

typedef enum
{
    TINY,
    SMALL,
    LARGE
} t_type;


/*
 *  Generic zone. Contains mandatory data for linked list and zone types.
 */
typedef struct s_generic_zone
{
    void *next;
    t_type type;
} t_generic_zone;


/*
 * Zone for Smalls and Tiny allocs. Contains a array of sizes (size 0 ==  free chunk),
 * and a memory block large enough to store BLOCKS_PER_ZONE contiguous blocks.
 *  <------------------------------  t_zone ------------------------------>
 *  | next |   type  |  info[]  |                  data...                |
 *                              | | | | | | | | | | | | | | | | | | | ... |
 *
*/

typedef struct s_zone
{
    void *next;                      /*  8 bytes */
    t_type type;                     /*  4 bytes */
    u_int16_t info[BLOCKS_PER_ZONE]; /*  2 * BLOCKS_PER_ZONE (246) */
} t_zone;

/*
 * Specific for larges allocations. The memory requested by the user is simply allocated in data[].
 *  <-----------------------------  t_large ------------------------------>
 *  | next |   type  |  size  |                  data...                  |
 *
*/
typedef struct s_large
{
    void *next;
    t_type type;
    size_t size;
} t_large;


typedef enum
{
    ENV_LOG = 1 << 1,
} t_env;


/*
 * libft_malloc functions
 */
void *malloc(size_t size);

void free(void *ptr);

void *realloc(void *ptr, size_t size);

void *calloc(size_t c, size_t size);

void show_alloc_mem();

void show_human_alloc_mem();


/*
 * Global utilities
 */
t_generic_zone **get_base_zone();

t_type get_zone_type(size_t size);

t_generic_zone *get_chunk_address(t_zone const *zone, size_t nb);

size_t get_total_user_size();

size_t get_total_size();

bool ptr_is_too_far(t_generic_zone const *zone, void const *ptr);

bool getenv_cached(t_env env);

/*
 * Display
 */
void print_type(t_type type, int fd);

void log_entry();

/*
 * Log
 */
void *log_malloc_return(void *ptr);
void log_malloc_entry(size_t size);
void log_free(void *ptr);
void log_free_invalid(void *ptr);
void log_realloc(void *ptr, size_t size);
void log_realloc_invalid(void *ptr);

void ft_putstr_log(char const*str);
