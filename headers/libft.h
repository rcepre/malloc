/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   libft.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 16:36:53 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 06:10:04 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LIBFT_H
#define LIBFT_H

#include <stdbool.h>
#include <unistd.h>

#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif

#ifdef NDEBUG
#define FT_ASSERT(condition)
#else
#define FT_ASSERT(condition) ft_assert((condition), __PRETTY_FUNCTION__, __FILE__, __LINE__)
#endif

#define FT_TEST(condition, msg) ft_test((condition), (msg), __PRETTY_FUNCTION__, __FILE__, __LINE__)


int ft_putchar(char c);
int ft_putchar_fd(char c, int fd);
int ft_putstr(char const *s);
int ft_putstr_fd(char const *s, int fd);
int ft_putendl(char const *s);
int ft_putendl_fd(char const *s, int fd);
int ft_putptr(void const *ptr);
int ft_putptr_fd(void const *ptr, int fd);
int ft_putnbr(long nb);
int ft_putnbr_fd(long n, int fd);
int ft_putsize_t(size_t nb);
int ft_putsize_t_fd(size_t n, int fd);

void ft_assert(bool condition, char const *function, char const *file, int line);
void ft_test(bool condition, char const *msg, char const *func, char const *file, int line);

char *ft_strcat(char *dest, const char *src);
size_t ft_strlen(const char *s);
int ft_strcmp(const char *s1, const char *s2);
char *ft_strcpy(char *dest, const char *src);

void ft_bzero(void *s, size_t n);
void *ft_memset(void *s, int c, size_t n);
void *ft_memcpy(void *dest, const void *src, size_t n);

size_t ft_max(size_t a, size_t b);
size_t ft_min(size_t a, size_t b);

#endif
