#include "../headers/libft.h"
#include "../headers/malloc.h"


void test_simple_allocs()
{
    FT_ASSERT(get_total_size() == 0);

    size_t user_total = 0;
    size_t size = 0;

    size = TINY_SIZE;
    void *tiny = malloc(size);
    user_total += size;
    FT_TEST(tiny != NULL, "Malloc return a ptr for tiny allocation.");
    FT_TEST(get_total_user_size() == user_total, "Good user total size value");
    FT_TEST(get_total_size() == PAGE_ALIGN(TINY_ZONE), "Good real total size value");

    size = SMALL_SIZE;
    void *small = malloc(SMALL_SIZE);
    user_total += size;
    FT_TEST(small != NULL, "Malloc return a ptr for small allocation.");
    FT_TEST(get_total_user_size() == user_total, "Good user total size value");
    FT_TEST(get_total_size() == PAGE_ALIGN(TINY_ZONE) + PAGE_ALIGN(SMALL_ZONE), "Good real total size value");

    size = 120000;
    void *large = malloc(size);
    user_total += size;
    FT_TEST(large != NULL, "Malloc return a ptr for large allocation.");
    FT_TEST(get_total_user_size() == user_total, "Good user total size value");
    FT_TEST(get_total_size() == PAGE_ALIGN(TINY_ZONE) + PAGE_ALIGN(SMALL_ZONE) + PAGE_ALIGN(size + sizeof(t_large)),
            "Good real total size value");

    free(tiny);
    FT_TEST(get_total_user_size() == SMALL_SIZE + size, "Total size == get_total_user_size(): tiny alloc is freed");

    free(large);
    FT_TEST(get_total_user_size() == SMALL_SIZE, "Total size == get_total_user_size(): large alloc is freed");

    free(small);
    FT_TEST(get_total_user_size() == 0, "Total size == 0: all allocs are freed");

    FT_TEST(*get_base_zone() == NULL, "All is free, base zone is set to NULL.");
}

void test_multiple(t_type type)
{
    FT_ASSERT(get_total_size() == 0);

    void *addresses[12000];
    size_t sizes[12000];
    size_t total_user_size = 0;
    bool ptr_is_null = false;
    size_t max_size = type == TINY ? TINY_SIZE : SMALL_SIZE;

    for (size_t i = 0; i < 12000; ++i)
    {
        sizes[i] = i % max_size;
        addresses[i] = malloc(sizes[i]);
        total_user_size += sizes[i];
        if (addresses[i] == NULL)
            ptr_is_null = true;
    }
    FT_TEST(ptr_is_null == false, "One of the allocations have failed.");

    for (int i = 200; i < 12000; ++i)
    { /* Free all except the 200 firsts elements. */
        free(addresses[i]);
        total_user_size -= sizes[i];
    }
    FT_TEST(total_user_size == get_total_user_size(), "Sizes are differents.");

    for (int i = 0; i < 200; ++i)
        free(addresses[i]);

    FT_TEST(get_total_user_size() == 0, "Total size == 0: all allocs are freed");

    FT_TEST(*get_base_zone() == NULL, "All is free, base zone is set to NULL.");
    //    show_alloc_mem();
}

void test_multiple_all_types()
{
    FT_ASSERT(get_total_size() == 0);

    size_t total_user_size = 0;
    bool ptr_is_null = false;
    size_t sizes[] = {12, 65465, 128, 64, 12, 1, 1, 1, 654864, 1, 646, 1, 64684, 42, 462, 666, 84864, 554, 6468465,
                      12, 128, 12, 64, 12, 1, 1, 1, 4444, 1, 646, 1, 1212, 42, 1, 666, 84864, 554, 666,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                      668, 0};
    int len = 0;
    while (sizes[len])
    {
        total_user_size += sizes[len];
        ++len;
    }
    void *addresses[len];

    for (int i = 0; i < len; ++i)
    {
        addresses[i] = malloc(sizes[i]);
        if (addresses[i] == NULL)
            ptr_is_null = true;
    }
    FT_TEST(ptr_is_null == false, "One of the allocations have failed.");
    FT_TEST(total_user_size == get_total_user_size(), "Sizes are differents.");
    for (int i = 0; i < len; ++i)
    {
        if (i % 3 == 0 || i % 4 == 0)
        {
            free(addresses[i]);
            total_user_size -= sizes[i];
        }
    }
    FT_TEST(total_user_size == get_total_user_size(), "Sizes are differents.");
    for (int i = 0; i < len; ++i)
    {
        if (i % 3 == 0 || i % 4 == 0)
        {
            addresses[i] = malloc(sizes[i]);
            total_user_size += sizes[i];
            if (addresses[i] == NULL)
                ptr_is_null = true;
        }
    }
    FT_TEST(ptr_is_null == false, "One of the allocations have failed.");
    FT_TEST(total_user_size == get_total_user_size(), "Sizes are differents.");

    for (int i = 0; i < len; ++i)
        free(addresses[i]);
}

void test_realloc()
{
    FT_ASSERT(get_total_size() == 0);


    size_t total_user_size = 0;
    bool ptr_is_null = false;
    size_t sizes[] = {42, 128, 42, 128, 1024, 2048, 42, 128, 4096, 2048, 21, 21, 1, 42, 0};
    int len = 0;
    while (sizes[len])
    {
        total_user_size += sizes[len];
        ++len;
    }
    void *addresses[len];
    for (int i = 0; i < len; ++i)
    {
        addresses[i] = malloc(sizes[i]);
        if (addresses[i] == NULL)
            ptr_is_null = true;
    }
    FT_TEST(ptr_is_null == false, "One of the allocations have failed.");
    FT_TEST(total_user_size == get_total_user_size(), "Sizes are differents.");

    for (int i = 0; i < len; ++i)
    {
        addresses[i] = realloc(addresses[i], sizes[i] * 2);
        if (addresses[i] == NULL)
            ptr_is_null = true;
    }
    FT_TEST(ptr_is_null == false, "One of the allocations have failed.");
    FT_TEST(total_user_size * 2 == get_total_user_size(), "Sizes are differents.");
    for (int i = 0; i < len; ++i)
    {
        realloc(addresses[i], 0);
    }
}

void test_realloc_fail()
{
    FT_ASSERT(get_total_size() == 0);

    char test_str[] = "Salut salut !! Je suis un test !\n";
    char test_str2[] = "Bla bla!\n";
    char *str, *tmp, *str2;

    str = realloc(NULL, SMALL_SIZE);
    FT_TEST(str, "ptr is not null");
    FT_TEST(get_total_user_size() == SMALL_SIZE, "User Size ok");
    FT_TEST(get_total_size() == PAGE_ALIGN(SMALL_ZONE), "A small zone has been allocated");

    ft_strcpy(str, test_str);
    FT_TEST(ft_strcmp(str, test_str) == 0, "str has been copied without segfault.");

    str2 = realloc(str, SMALL_SIZE * 2);
    FT_TEST(str2, "ptr is not null");
    FT_TEST(get_total_user_size() == SMALL_SIZE * 2, "User Size ok");
    FT_TEST(ft_strcmp(str2, test_str) == 0, "str has been copied by realloc.");

    ft_strcpy(str2, test_str2);
    FT_TEST(ft_strcmp(str2, test_str2) == 0, "str has been copied without segfault.");


    FT_TEST(realloc(str, 12) == NULL, "Realloc ruturn NULL (invalid pointer).");

    tmp = str2;
    str2 = realloc(str2, 12);
    FT_TEST(get_total_user_size() == 12, "User Size ok");
    FT_TEST(get_total_size() == PAGE_ALIGN(TINY_ZONE), "Size ok");
    FT_TEST(str2 != tmp, "Address has changed due to the change of allocation types (LARGE to TINY).");

    tmp = str2;
    str2 = realloc(str2, 64);
    FT_TEST(str2 == tmp, "Address stay the same. Just size change");
    FT_TEST(get_total_user_size() == 64, "User Size ok");
    free(str2);
}

int main()
{
//    log_entry();
//    test_simple_allocs();
//    test_multiple(TINY);
//    test_multiple(SMALL);
//    test_multiple_all_types();
//    test_realloc();
//    test_realloc_fail();
//void *p =malloc(22);
ft_putstr_fd("MAIN\n", 2);
    char *p = calloc(2, 4);
    p[2] = 2;
    realloc(p, 33);
    free(p);
//    show_human_alloc_mem();
    return (0);
}