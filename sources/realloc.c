#include "malloc.h"

void log_zone_realloced(t_generic_zone* zone, int i)
{
    if (!getenv_cached(ENV_LOG)) return;

    size_t size;

    if (zone->type == LARGE)
        size = ((t_large*)zone)->size;
    else
        size =  ((t_zone *) zone)->info[i];

    ft_putstr_fd("Realloc a  ", 2);
    print_type(zone->type, 2);
    ft_putstr_fd(" zone of size ", 2);
    ft_putsize_t_fd(size, 2);

}

void *realloc(void *ptr, size_t size)
{
    t_generic_zone **list = get_base_zone();
    t_generic_zone *zone = *list;

    log_realloc(ptr, size);
    if (ptr == NULL)
        return malloc(size);
    if (ptr != NULL && size == 0) {
        free(ptr);
        return NULL;
    }

    while (zone)
    {
        if (zone->type == LARGE && ptr == LARGE_OFFSET(zone))
        {
            t_large *new = malloc(size);
            ft_memcpy(new, LARGE_OFFSET(zone), ft_min(size, ((t_large *) zone)->size));
            free(ptr);
            log_zone_realloced(zone, 0);
            ft_putstr_log("\n");
            return new;
        }
        else if (zone->type != LARGE && !ptr_is_too_far(zone, ptr))
        {
            for (unsigned int i = 0; i < BLOCKS_PER_ZONE; ++i)
            {
                if (ptr == get_chunk_address((t_zone *) zone, i))
                {
                    if (zone->type == get_zone_type(size))
                    {
                        log_zone_realloced(zone, i);
                        ft_putstr_log("\n");
                        return ptr;
                    }
                    else
                    {
                        t_zone *new = malloc(size);
                        ft_memcpy(new, get_chunk_address((t_zone *) zone, i), ft_min(size, ((t_zone *) zone)->info[i]));
                        free(ptr);
                        log_zone_realloced(zone, i);
                        ft_putstr_log("  REUSE ZONE\n");
                        return new;
                    }
                }
            }
        }
        zone = zone->next;
    }
    log_realloc_invalid(ptr);
    return NULL;
}