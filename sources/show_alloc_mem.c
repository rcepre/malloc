#include "malloc.h"

static void print_large_zone(t_large *zone)
{
    void *addr = LARGE_OFFSET(zone);

    ft_putptr(addr);
    ft_putstr(" - ");
    ft_putptr(addr + zone->size);
    ft_putstr(" : ");
    ft_putsize_t(zone->size);
    ft_putstr(" octets \n");
}

static void print_zone(t_zone *zone)
{
    for (unsigned int i = 0; i < BLOCKS_PER_ZONE; ++i)
    {
        if (zone->info[i] != 0)
        {
            void *addr = get_chunk_address(zone, i);
            ft_putptr(addr);
            ft_putstr(" - ");
            ft_putptr(addr + zone->info[i]);
            ft_putstr(" : ");
            ft_putsize_t(zone->info[i]);
            ft_putstr(" octets \n");
        }
    }
}

void show_alloc_mem()
{
    t_generic_zone **zones = get_base_zone();
    t_generic_zone *start = *zones;

    while (start)
    {
        print_type(start->type, 1);
        ft_putstr(" : ");
        ft_putptr(start);
        ft_putstr("\n");

        if (start->type != LARGE)
            print_zone((t_zone *) start);
        else
            print_large_zone((t_large *) start);
        start = start->next;
    }
}
