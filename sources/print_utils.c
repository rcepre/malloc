#include "malloc.h"

void print_type(t_type type, int fd)
{
    if (type == LARGE)
        ft_putstr_fd("LARGE", fd);
    if (type == SMALL)
        ft_putstr_fd("SMALL", fd);
    if (type == TINY)
        ft_putstr_fd("TINY ", fd);
}