#include "malloc.h"

size_t get_total_user_size()
{
    t_generic_zone *start = *get_base_zone();
    size_t total = 0;

    while (start)
    {
        if (start->type != LARGE)
        {
            size_t sub_total = 0;
            for (unsigned int i = 0; i < BLOCKS_PER_ZONE; ++i)
                sub_total += ((t_zone *) start)->info[i];
            ;
            total += sub_total;
        }
        else
        {
            total += ((t_large *) start)->size;
        }
        start = start->next;
    }
    return total;
}

size_t get_total_size()
{
    t_generic_zone *start = *get_base_zone();
    size_t total = 0;

    while (start)
    {
        if (start->type != LARGE)
        {
            total += PAGE_ALIGN(TYPE_TO_SIZE(start->type));
        }
        else
        {
            total += PAGE_ALIGN(((t_large *) start)->size + sizeof(t_large));
        }
        start = start->next;
    }
    return total;
}