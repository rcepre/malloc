
#include "malloc.h"
#include <fcntl.h>


static void log_zone_info(t_type type)
{

    size_t zone_size = type == TINY ? TINY_ZONE : SMALL_ZONE;


    ft_putstr_fd("\n", 2);
    print_type(TINY, 2);
    ft_putstr_fd(":\n\tblock size: ", 2);
    ft_putnbr_fd(type == TINY ? TINY_SIZE : SMALL_SIZE, 2);
    ft_putstr_fd("\n\tzone size: ", 2);
    ft_putnbr_fd(zone_size, 2);
    ft_putstr_fd("\n\treal_size: ", 2);
    ft_putnbr_fd(PAGE_ALIGN(zone_size), 2);
    ft_putstr_fd("\n\tlost: ", 2);
    ft_putnbr_fd(PAGE_ALIGN(zone_size) - zone_size, 2);
    ft_putstr_fd("  ( + ", 2);
    ft_putnbr_fd(sizeof(t_zone), 2);
    ft_putstr_fd(" )", 2);
};

void log_entry()
{
    static bool done = false;

    if (done || !getenv_cached(ENV_LOG))
        return;
    done = true;

    ft_putstr_fd("=============================== libft_malloc LOG ===============================\n", 2);

#ifndef NDEBUG
    ft_putstr_fd("[ DEBUG BUILD ]\n", 2);
#endif

    ft_putstr_fd("zone size: ", 2);
    ft_putnbr_fd(sizeof(t_zone), 2);
    ft_putstr_fd("\nblocks per zones: ", 2);
    ft_putnbr_fd(BLOCKS_PER_ZONE, 2);

    log_zone_info(TINY);
    log_zone_info(SMALL);
    ft_putstr_fd("\n--------------------------------------------------------------------------------\n", 2);

}

void log_malloc_entry(size_t size)
{
    if (!getenv_cached(ENV_LOG)) return;

    ft_putstr_fd("malloc(", 2);
    ft_putsize_t_fd(size, 2);
    ft_putstr_fd(")\n", 2);
}

void *log_malloc_return(void *ptr)
{
    if (!getenv_cached(ENV_LOG)) return ptr;

    ft_putstr_fd("malloc return ", 2);
    ft_putptr_fd(ptr, 2);
    ft_putstr_fd("\n", 2);
    return ptr;
}

void log_free(void *ptr)
{
    if (!getenv_cached(ENV_LOG)) return;

    if (ptr)
    {
        ft_putstr_fd("free(", 2);
        ft_putptr_fd(ptr, 2);
        ft_putstr_fd(") : ", 2);
    }
    else
        ft_putstr_fd("free(0)\n", 2);
}

void log_free_invalid(void *ptr)
{
    if (!getenv_cached(ENV_LOG)) return;
    ft_putstr_fd("Free: Invalid ptr: ", 2);
    ft_putptr_fd(ptr, 2);
    ft_putstr_fd("\n", 2);
}

void log_realloc(void *ptr, size_t size)
{
    if (!getenv_cached(ENV_LOG)) return;

    ft_putstr_fd("realloc(", 2);
    ft_putptr_fd(ptr, 2);
    ft_putstr_fd(", ", 2);
    ft_putsize_t_fd(size, 2);
    ft_putstr_fd(")\n", 2);
}

void log_realloc_invalid(void *ptr)
{
    if (!getenv_cached(ENV_LOG)) return;
    ft_putstr_fd("Realloc: Invalid ptr: ", 2);
    ft_putptr_fd(ptr, 2);
    ft_putstr_fd("\n", 2);
}

void ft_putstr_log(char const*str)
{
    if (!getenv_cached(ENV_LOG)) return;
    ft_putstr_fd(str, 2);
}