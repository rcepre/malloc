#include "malloc.h"

static void put_empty_blocks(int emptys) {
    if (emptys > 2)
    {
        ft_putstr(" <");
        ft_putsize_t(emptys);
        ft_putstr("> ");
    }
    else
    {
        for (int j = 0; j < emptys; ++j)
            ft_putstr("[]");
    }
}

static void print_blocks(t_zone const *zone)
{
    int emptys = 0;

    for (unsigned int i = 0; i < BLOCKS_PER_ZONE; ++i)
    {
        if ((*zone).info[i] == 0)
            emptys++;
        else
        {
            put_empty_blocks(emptys);
            ft_putstr("[");
            ft_putsize_t(zone->info[i]);
            ft_putstr("]");
            emptys = 0;
        }
    }
    put_empty_blocks(emptys);
}

static void display_zone(t_generic_zone const *zone)
{
    FT_ASSERT(zone != NULL);
    ft_putptr(zone);
    ft_putstr(" : ");
    print_type(zone->type, 1);
    if (zone->type == LARGE)
    {
        ft_putstr(" ( ");
        ft_putsize_t(((t_large *) zone)->size);
        ft_putstr(" ) ");
    }

    if (zone->type != LARGE)
    {
        ft_putstr(" Blocks:  ");
        print_blocks((t_zone *) zone);
    }
    ft_putstr("\n");
}


static void put_format_kio(size_t n)
{
    if (n > 4096)
    {
        ft_putsize_t(n / 1024);
        ft_putstr("kio (");
        ft_putsize_t(n);
        ft_putstr(")");

    }
    else
    {
        ft_putsize_t(n);
    }
}

void show_human_alloc_mem()
{
    t_generic_zone **zones = get_base_zone();
    t_generic_zone *start = *zones;

    size_t total_user_size = get_total_user_size();
    size_t total_size = get_total_size();
    size_t lost = (total_size - total_user_size);

    while (start)
    {
        display_zone(start);
        start = start->next;
    }
    ft_putstr("TOTAL USER SIZE: ");
    put_format_kio(total_user_size);
    ft_putstr(" - TOTAL SIZE: ");
    put_format_kio(total_size);
    ft_putstr(" - LOST: ");
    put_format_kio(lost);


    ft_putstr("\n");
}