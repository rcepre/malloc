#include <stdlib.h>
#include "malloc.h"

/*
** We cache the environment variables in the first call
** https://github.com/xianyi/OpenBLAS/issues/716
*/

bool	getenv_cached(t_env env)
{
    static u_int32_t env_cache;
    static bool	init;

    if (!init)
    {
        if (getenv("MALLOC_LOG"))
            env_cache |= ENV_LOG;
        init = true;
    }
    return (env & env_cache);
}