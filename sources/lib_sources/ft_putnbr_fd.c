/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   putnbr.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/09 12:32:14 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/10/04 11:16:06 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../headers/libft.h"

int	ft_putnbr_fd(long nb, int fd)
{
	int nb_copy;


	if (nb < 0)
	{
		if (!ft_putchar_fd('-', 1))
            return 0;
		nb = -nb;
	}
	nb_copy = nb;
	if (nb_copy > 9)
	{
        size_t ret = 0;
		ret += ft_putnbr_fd(nb_copy / 10, fd);
		ret += ft_putnbr_fd(nb_copy % 10, fd);
        if (!ret) return 0;
	}
	else {
        if (!ft_putchar_fd(nb_copy + '0', fd))
            return 0;
    }
    return 1;
}
