/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   putnbr.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/09 12:32:14 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 18:00:18 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../headers/libft.h"

int     ft_putsize_t_fd(size_t nb, int fd)
{
    size_t nb_copy;

    nb_copy = nb;
    if (nb_copy > 9)
    {
        int ret = 0;
        ret += ft_putsize_t_fd(nb_copy / 10, fd);
        ret += ft_putsize_t_fd(nb_copy % 10, fd);
        if (!ret) return 0;
    }
    else {
        if (!ft_putchar_fd(nb_copy + '0', fd))
            return 0;
    }
    return 1;
}
