#include "../../headers/libft.h"

int ft_putptr(void const *ptr)
{
    return ft_putptr_fd(ptr, 1);
}