#include "../../headers/libft.h"
#include <stdlib.h>

void ft_assert(bool condition, char const *function, char const *file, int line)
{
    if (!condition) {
        ft_putstr_fd("Assertion failed: in file ", 2);
        ft_putstr_fd(file, 2);
        ft_putstr_fd(" in function ", 2);
        ft_putstr_fd(function, 2);
        ft_putstr_fd("(),  at line  ", 2);
        ft_putnbr_fd(line, 2);
        ft_putstr_fd("\n", 2);
        exit(1);
    }
}