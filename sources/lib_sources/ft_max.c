/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_max.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2021/01/08 10:28:34 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2021/01/08 10:34:08 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../headers/libft.h"

size_t ft_max(size_t a, size_t b)
{
    if (a > b)
        return a;
    return b;
}

size_t ft_min(size_t a, size_t b)
{
    if (a < b)
        return a;
    return b;
}