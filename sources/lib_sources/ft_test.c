#include "../../headers/libft.h"

void ft_test(bool condition, char const *msg, char const *func, char const *file, int line)
{
    if (!condition) {
        ft_putstr_fd("Test failed: in file ", 2);
        ft_putstr_fd(file, 2);
        ft_putstr_fd(" in function ", 2);
        ft_putstr_fd(func, 2);
        ft_putstr_fd("(),  at line  ", 2);
        ft_putnbr_fd(line, 2);
        ft_putstr_fd(" => ", 2);
        ft_putstr_fd(msg, 2);
        ft_putstr_fd("\n", 2);
    }
}