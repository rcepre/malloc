#include "../../headers/libft.h"

int ft_putptr_fd(void const *ptr, int fd)
{
    long int div;
    char temp;
    unsigned long int n;

    n = (unsigned long int)ptr;
    div = 16;
    if (!ft_putstr_fd("0x", fd)) return 0;
    while (n / div >= 16)
        div *= 16;
    while (div > 0)
    {
        temp = '0' + n / div;
        if (temp > '9')
        {
            temp += 39;
            if (!ft_putchar_fd(temp, fd)) return 0;
        }
        else
        if (!ft_putchar_fd(temp, fd)) return 0;
        n %= div;
        div /= 16;
    }
    return 1;
}