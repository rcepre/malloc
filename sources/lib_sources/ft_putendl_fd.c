/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_putstr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 16:39:12 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 19:00:49 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../headers/libft.h"

int	ft_putendl_fd(const char *s, int fd)
{
    size_t ret = 0;
    ret += ft_putstr_fd(s, 1);
	ret += ft_putchar_fd('\n', fd);
    return ret;
}
