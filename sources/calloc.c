

#include "malloc.h"

void *calloc(size_t c, size_t size)
{
    void *ptr;

    ptr = malloc(c * size);
    ft_memset(ptr, 0, c * size);
    return ptr;
}