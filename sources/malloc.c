#include "malloc.h"

static void link_zone(t_generic_zone *zone, t_generic_zone **list)
{
    t_generic_zone *start = *list;

    if (!*list)
    {
        *list = zone;
        return;
    }
    if (start > zone)
    {
        zone->next = start;
        start = zone;
    }
    else
    {
        while ((*list)->next != NULL && (*list)->next >= (void *) zone)
            (*list) = (*list)->next;

        zone->next = (*list)->next;
        (*list)->next = zone;
    }
    *list = start;
}

static size_t get_zone_size(size_t chunk_size)
{
    t_type type = get_zone_type(chunk_size);

    if (type == TINY)
        return TINY_ZONE;
    else if (type == SMALL)
        return SMALL_ZONE;
    else
        return chunk_size + sizeof(t_zone);
}

static int first_free_chunk(t_zone const *zone)
{
    for (unsigned int i = 0; i < BLOCKS_PER_ZONE; i++)
        if (zone->info[i] == 0)
            return i;
    return FULL;
}

static t_generic_zone *create_zone(size_t size)
{

    t_generic_zone *zone;
    size_t zone_size = get_zone_size(size);

    FT_ASSERT(PAGE_ALIGN(zone_size) % getpagesize() == 0);
    ft_putstr_log("[ call mmap() ] ");
    zone = mmap(NULL, PAGE_ALIGN(zone_size), MMAP_FLAGS);

    if (zone == MAP_FAILED)
    {
        ft_putstr_fd("MMAP_FAILED code ", 2);
        ft_putnbr_fd(errno, 2);
        ft_putstr_fd("\n", 2);
    }

    t_type type = get_zone_type(size);

    zone->type = type;
    zone->next = NULL;

    if (type == LARGE)
        ((t_large *) zone)->size = size;
    else
        ft_bzero(((t_zone *) zone)->info, BLOCKS_PER_ZONE * sizeof(u_int16_t));

    ft_putstr_fd("create_zone ", 2);
    print_type(zone->type, 2);
    ft_putstr_fd(" @", 2);
    ft_putptr_fd(zone, 2);
    ft_putstr_fd("\n", 2);


    return zone;
}


static t_generic_zone *find_free_space(t_generic_zone *list, size_t size)
{
    t_type type = get_zone_type(size);

    if (type == LARGE)
        return NULL;

    while (list)
    {
        if (list->type == type && (first_free_chunk((t_zone *) list)) != FULL)
            return list;
        list = list->next;
    }
    return NULL;
}

static void *alloc_chunk(t_generic_zone **zone, size_t size)
{
    FT_ASSERT(first_free_chunk((t_zone *) *zone) != FULL); /* must have at least one free chunk */

    /* For larges allocs, don't need to check where are free chunks. */
    if ((*zone)->type == LARGE)
        return LARGE_OFFSET(*zone);

    int idx = first_free_chunk((t_zone *) *zone);

    ((t_zone *) *zone)->info[idx] = size;
    return get_chunk_address((t_zone *) *zone, idx);
}


void *malloc(size_t size)
{
    log_malloc_entry(size);
    log_entry();
    t_generic_zone *space = NULL;
    t_generic_zone **zones = get_base_zone();

    if (!(space = find_free_space(*zones, size)))
    {
        if (!(space = create_zone(size)))
            return NULL;
        link_zone(space, zones);
    }

    void *ret = alloc_chunk(&space, size);
    return log_malloc_return(ret);
}
