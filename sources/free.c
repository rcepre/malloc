#include "malloc.h"

static bool empty_zone(t_zone const *zone)
{
    FT_ASSERT(zone->type != LARGE);
    for (unsigned int i = 0; i < BLOCKS_PER_ZONE; ++i)
        if (zone->info[i])
            return false;
    return true;
}

static size_t size_to_free(t_generic_zone *zone)
{
    size_t size;

    if (zone->type != LARGE)
        size = TYPE_TO_SIZE(zone->type);
    else
        size = ((t_large *) zone)->size + sizeof(t_large);
    return PAGE_ALIGN(size);
}

void log_unlink_zone(t_generic_zone const *zone, size_t size)
{
    if (!getenv_cached(ENV_LOG)) return;
    ft_putstr_fd(" [ Remove  ", 2);
    print_type(zone->type, 2);
    ft_putstr_fd(" zone size  ", 2);
    ft_putsize_t_fd(size, 2);
    ft_putstr_fd(", @", 2);
    ft_putptr_fd(zone, 2);
    ft_putstr_fd(" ] ", 2);
}

static void unlink_zone(t_generic_zone *zone, t_generic_zone **list)
{
    size_t size;
    t_generic_zone *start = *list;

    if (*list != zone)
    {
        while ((*list)->next != zone)
            *list = (*list)->next;
        (*list)->next = zone->next;
    }

    if (zone == *list)
        *list = zone->next;
    else
        *list = start;

    size = size_to_free(zone);
    log_unlink_zone(zone, size);
    ft_putstr_log(" [ call mmap() ] ");
    if (munmap(zone, size) == -1)
    {
        ft_putstr_fd("MUNMAP_FAILED code ", 2);
        ft_putnbr_fd(errno, 2);
        ft_putstr_fd("\n", 2);
        return;
    }
    zone = NULL;
}

void log_zone_freed(t_generic_zone* zone, int i)
{
    if (!getenv_cached(ENV_LOG)) return;

    size_t size;

    if (zone->type == LARGE)
        size = ((t_large*)zone)->size;
    else
        size =  ((t_zone *) zone)->info[i];

    ft_putstr_fd("Free a  ", 2);
    print_type(zone->type, 2);
    ft_putstr_fd(" chunk of size ", 2);
    ft_putsize_t_fd(size, 2);
    ft_putstr_fd(" @", 2);
    if (zone->type == LARGE)
        ft_putptr_fd(LARGE_OFFSET(zone), 2);
    else
        ft_putptr_fd(get_chunk_address((t_zone *) zone, i), 2);

}

void free(void *ptr)
{
    log_free(ptr);
    if (!ptr)
        return;

    t_generic_zone **list = get_base_zone();
    t_generic_zone *zone = *list;

    while (zone)
    {
        if (zone->type == LARGE && ptr == LARGE_OFFSET(zone))
        {
            log_zone_freed(zone, 0);
            unlink_zone(zone, list);
            ft_putstr_log("\n");
            return;
        }
        else if (zone->type != LARGE && !ptr_is_too_far(zone, ptr))
        {
            for (unsigned int i = 0; i < BLOCKS_PER_ZONE; ++i)
            {
                if (ptr == get_chunk_address((t_zone *) zone, i))
                {
                    log_zone_freed(zone, i);
                    ((t_zone *) zone)->info[i] = 0;
                    if (empty_zone((t_zone *) zone))
                        unlink_zone(zone, list);
                    ft_putstr_log("\n");
                    return;
                }
            }
        }
        zone = zone->next;
    }
    log_free_invalid(ptr);
}
