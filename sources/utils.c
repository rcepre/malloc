#include "malloc.h"

t_generic_zone **get_base_zone()
{
    static t_generic_zone *base_zone = NULL;
    return &base_zone;
}


t_type get_zone_type(size_t chunk_size)
{
    if (chunk_size > SMALL_SIZE)
        return LARGE;
    if (chunk_size > TINY_SIZE)
        return SMALL;
    return TINY;
}

static size_t get_chunk_size(t_type t)
{
    FT_ASSERT(t != LARGE); /* Larges allocations are not chunks */
    if (t == SMALL)
        return SMALL_SIZE;
    return TINY_SIZE;
}

t_generic_zone *get_chunk_address(t_zone const *zone, size_t nb)
{
    FT_ASSERT(zone && zone->type != LARGE); /* Larges allocations are not chunks */

    size_t delta = (get_chunk_size((*zone).type)) * nb;

    FT_ASSERT(delta + sizeof(t_zone) < TYPE_TO_SIZE(zone->type));
    return ZONE_OFFSET(zone) + delta;
}



bool ptr_is_too_far(t_generic_zone const *zone, void const *ptr)
{
    if ((zone->type == TINY && ptr - (void *) zone >= (long) TINY_ZONE) ||
        (zone->type == SMALL && ptr - (void *) zone >= (long) SMALL_ZONE))
    {
        return true;
    }
    return false;
}